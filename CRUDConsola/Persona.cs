﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDConsola
{
    public class Persona
    {
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { 
                nombre = value;
            }
        }

        public char Sexo { get; set; }
        public int Edad { get; set; }
        public string Email { get; set; }

        public Persona(string nombre, char genero, int edad, string email)
        {
            //inicializando los valores de mis propiedades
            Nombre = nombre;
            Sexo = genero;
            Edad = edad;
            Email = email;
            
        }
        /// <summary>
        /// Incrementa la edad en 1
        /// </summary>
        public void Cumple()
        {
            Edad++;
        }
        /// <summary>
        /// Suma dos numeros enterios
        /// </summary>
        /// <param name="a">primero numero a sumar</param>
        /// <param name="b">segundo numero a sumar</param>
        /// <returns>Suma de los numeros dados</returns>
        public int Operacion(int a, int b)
        {
            return a+b;
        }
    }
}
