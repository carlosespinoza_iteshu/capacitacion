﻿
using CRUDConsola;
List<Persona> personas = new List<Persona>();
int opcion = 0;
do
{
    Console.Clear();
    Console.WriteLine("Menu");
    Console.WriteLine("1. Insertar");
    Console.WriteLine("2. Modificar");
    Console.WriteLine("3. Eliminar");
    Console.WriteLine("4. Mostrar");
    Console.WriteLine("9. Salir");
    Console.Write("Opción: ");

    if (int.TryParse(Console.ReadLine(), out opcion))
    {
        switch (opcion)
        {
            case 1:
                Insertar();
                break;
            case 2:
                Modificar();
                break;
            case 3:
                Eliminar();
                break;
            case 4:
                Mostrar();
                break;
            case 9:
                break;
            default:
                Console.WriteLine("Opción no encontrada");
                break;
        }
    }
    else
    {
        Console.WriteLine("No se puede procesar la opción");
    }
    Console.ReadLine();
} while (opcion != 9);

void Mostrar()
{
    Console.WriteLine("Nombre\tEdad\tGenero\tEmail");
    foreach (var item in personas)
    {
        Console.WriteLine($"{item.Nombre}\t{item.Edad}\t{item.Sexo}\t{item.Email}");
    }
    Console.WriteLine($"Registros encontrados: {personas.Count}");
}

void Eliminar()
{
    Mostrar();
    Console.Write("Nombre de la persona a eliminar: ");
    string nombre = Console.ReadLine();
    try
    {
        Persona persona = personas.Where(p => p.Nombre == nombre).SingleOrDefault();
        if (persona != null)
        {
            personas.Remove(persona);
            Console.WriteLine("Registro eliminado");
        }
        else
        {
            Console.WriteLine("No hay elementos con ese nombre...");
        }
    }
    catch (Exception)
    {
        Console.WriteLine("hay mas de uno...");
    }
}

void Modificar()
{
    Mostrar();
    Console.WriteLine("Nombre de la persona a modificar: ");
    string nombre = Console.ReadLine();
    try
    {
        Persona persona = personas.Where(p => p.Nombre == nombre).SingleOrDefault();
        if (persona != null)
        {
            Console.Write("Nombre: ");
            persona.Nombre = Console.ReadLine();
            Console.Write("Edad: ");
            persona.Edad = int.Parse(Console.ReadLine());
            Console.Write("Genero [M/F]: ");
            persona.Sexo = Console.ReadLine()[0];
            Console.Write("Email: ");
            persona.Email = Console.ReadLine();
            Console.WriteLine("Registro Modificado");
        }
        else
        {
            Console.WriteLine("No hay elementos con ese nombre...");
        }
    }
    catch (Exception)
    {
        Console.WriteLine("Hay mas de uno...");
    }
}

void Insertar()
{
    Console.Write("Nombre: ");
    string nombre = Console.ReadLine();
    Console.Write("Edad: ");
    int edad = int.Parse(Console.ReadLine());
    Console.Write("Genero [M/F]: ");
    char genero = Console.ReadLine()[0];
    Console.Write("Email: ");
    string email = Console.ReadLine();
    personas.Add(new Persona(nombre, genero, edad, email));
    Console.WriteLine("Registro insertado!!!");
}